<?php

require_once ('../vendor/autoload.php');

// load .env
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..', '.env.dev');
$dotenv->load();
$dotenv->required(['DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS', 'ALLOW_ORIGIN']);

$router = new \Bramus\Router\Router();
$router->setNamespace('\Http');

// add your routes and run!
$router->get('/', function() {
    readfile('home.html'); // we don't need Twig here ...
});

$router->mount('/api', function() use ($router) {
    $router->get('/tasks', 'TaskController@overview');

});

// CORS: support for preflight requests
$router->options('/api/tasks(/\d+)?', function() {
    header('Access-Control-Allow-Origin: ' . $_ENV['ALLOW_ORIGIN']);
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, Authorization');
    header('Access-Control-Allow-Credentials: true');
});

$router->run();