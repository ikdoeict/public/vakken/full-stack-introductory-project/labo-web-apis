<?php

namespace Http;

use Services\DatabaseConnector;

class TaskController extends ApiBaseController
{
    protected \Doctrine\DBAL\Connection $db;

    /**
     * TaskController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        // initiate DB connection
        $this->db = DatabaseConnector::getConnection();
    }

    public function overview()
    {
        $taskRows = $this->db->fetchAllAssociative('SELECT * FROM tasks ORDER BY priority', []);
        echo json_encode(['tasks' => $taskRows]);
    }

}