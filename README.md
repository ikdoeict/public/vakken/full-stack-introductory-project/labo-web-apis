# Labo Web APIs
Startoplossing voor het labo Web APIs (OPO *Full-stack: Introductory Project*).

We verwachten 1 afgewerkt labo per projectgroep.

## Hoe ga ik aan de slag met dit labo (eenvoudige manier)
1. Maak een fork van deze repo (knop 'Forks' rechtsboven), met dezelfde naam, in de namespace ikdoeict/<uw-naam> en met visibility Private(!)
2. Clone de fork op jouw systeem
```shell
git clone https://gitlab.com/ikdoeict/<your-name>/labo-web-apis.git
```
3. Open de map in je favoriete IDE, maak het labo en stage, commit en push voortdurend
4. **Dien jij in? Vergeet de repo niet te delen met je docent(en).**

**Opmerking**: je kan maar 1 fork aanmaken van een repo, tenzij je de fork-relatie verwijdert in de Settings

## Hoe ga ik aan de slag met dit labo (voor pro's)

1. Create a new completely! empty project (without README) on gitlab.com/ikdoeict, for example labo-web-apis
2. Execute following commands on your system (pay attention !)
```shell
mkdir labo-web-apis
cd labo-web-apis
git init
git pull https://gitlab.com/ikdoeict/public/vakken/full-stack-introductory-project/labo-web-apis.git
git remote add origin https://gitlab.com/ikdoeict/<your-name>/labo-web-apis.git
git push -u origin master
```
3. From now on, you can stage, commit and push as usual.
4. Open the main folder with an IDE (such as PhpStorm or Visual Studio Code)

## Links

* [Course slides](https://ikdoeict.gitlab.io/public/vakken/back-end-development/workshops/)
* [PHP Documentation](https://www.php.net/docs.php)
* [MySQL 8.4 Reference Manual](https://dev.mysql.com/doc/refman/8.4/en/)
* [Doctrine DBAL 4.2 documentation](https://www.doctrine-project.org/projects/doctrine-dbal/en/4.2/index.html)
* [Twig 3.x documentation](https://twig.symfony.com/doc/3.x/)
* [bramus/router documentation](https://github.com/bramus/router)

## Running and stopping the Docker MCE

* Run the environment, using Docker, from your terminal/cmd
```shell
cd <your-project>
docker-compose up
```
* Stop the environment in your terminal/cmd by pressing <code>Ctrl+C</code>
* In order to avoid conflicts with your lab/slides environment, run from your terminal/cmd
```shell
docker-compose down
```

## Installing phpdotenv, DBAL and bramus/router

The MCE is provided with a `composer.json`/`composer.lock` file, providing the phpdotenv, DBAL and bramus/router libraries
* In order to install, run from your terminal/cmd
```shell
docker-compose exec php-web bash
$ composer install
$ exit
```

## About the autoloader

`composer.json` is configured such that the classes in "src/" (and subfolders) are autoloaded.
* This means there is no need to require these classes anymore in your `public/*.php` scripts.
* You can extend this list yourself in `composer.json`
* When you changed this list, or you created some new classes, let composer know from your terminal/cmd:
```shell
docker-compose exec php-web bash
$ composer dump-autoload
$ exit
```

## Recipes and troubleshooting

### <code>docker-compose up</code> does not start one or more containers
* Look at the output of <code>docker-compose up</code>. When a container (fails and) exits, it is shown as the last line of the container output (colored tags by container)
* Alternatively, start another terminal/cmd and inspect the output of <code>docker-compose ps -a</code>. You can see which container exited, exactly when.
* Probably one of the containers fails because TCP/IP port 8000, 8080 or 3307 is already in use on your system. Stop the environment, change the port in <code>docker-compose.yml</code> en rerun <code>docker-compose up</code>.


